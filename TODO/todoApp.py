#! /usr/bin/python3
import os # import for clear screen - linux terminal 'clear' usage
# To do list
# 2018.10.27. - 2018.10.28.

""" *******************************************
    ***     outer module for read  1 KEY    ***
    *******************************************    """
def read_single_keypress():
    """Waits for a single keypress on stdin.

    This is a silly function to call if you need to do it a lot because it has
    to store stdin's current setup, setup stdin for reading single keystrokes
    then read the single keystroke then revert stdin back after reading the
    keystroke.

    Returns the character of the key that was pressed (zero on
    KeyboardInterrupt which can happen when a signal gets handled)

    """
    import termios, fcntl, sys, os
    fd = sys.stdin.fileno()
    # save old state
    flags_save = fcntl.fcntl(fd, fcntl.F_GETFL)
    attrs_save = termios.tcgetattr(fd)
    # make raw - the way to do this comes from the termios(3) man page.
    attrs = list(attrs_save) # copy the stored version to update
    # iflag
    attrs[0] &= ~(termios.IGNBRK | termios.BRKINT | termios.PARMRK 
                  | termios.ISTRIP | termios.INLCR | termios. IGNCR 
                  | termios.ICRNL | termios.IXON )
    # oflag
    attrs[1] &= ~termios.OPOST
    # cflag
    attrs[2] &= ~(termios.CSIZE | termios. PARENB)
    attrs[2] |= termios.CS8
    # lflag
    attrs[3] &= ~(termios.ECHONL | termios.ECHO | termios.ICANON
                  | termios.ISIG | termios.IEXTEN)
    termios.tcsetattr(fd, termios.TCSANOW, attrs)
    # turn off non-blocking
    fcntl.fcntl(fd, fcntl.F_SETFL, flags_save & ~os.O_NONBLOCK)
    # read a single keystroke
    try:
        ret = sys.stdin.read(1) # returns a single character
    except KeyboardInterrupt: 
        ret = '\x03'
    finally:
        # restore old state
        termios.tcsetattr(fd, termios.TCSAFLUSH, attrs_save)
        fcntl.fcntl(fd, fcntl.F_SETFL, flags_save)
    return ret

""" *******************************************
    ***     PROGRAM DEF-s and PROGRAM       ***
    *******************************************    """

def get_todolist_from_file(): 
    
    with open('todo_items.txt') as f:
        local_todolist = f.read().splitlines()
        return local_todolist

def get_checklist_from_file():
    
    with open('checkList.txt') as f2:
        local_checklist_string = f2.read().splitlines()
        return local_checklist_string
    
def print_main_items(todolist, checklist):

    for x in range(len(todolist)):
        print("║ %d. \t %s \t %s" % (x+1,  checklist[x] , todolist[x]))
    print ("╠════════════════════════════════════════════════════════════════════╣")
    print ("║ Feladat hozzáadása:               (+ billentyü)                    ║")
    print ("║ Feladat jelölése                  (üss egy számot)                 ║")
    print ("║ Kilépés                           (q = quit)                       ║")
    print ("╠════════════════════════════════════════════════════════════════════╣")

def handle_keypress(todolist,checklist):
    # **THIS METHOD PURPOSE IS TO HANDLE THE KEYBOARD KEYPRESSES **
    
    #menukeypress = input("\nUss le billentyut --> ") # waiting for input, and put it into menukeypress variable
    print ("║                                        Üss egy billentyüt -->      ║")
    print ("╚════════════════════════════════════════════════════════════════════╝")
    menukeypress = read_single_keypress()
    
    #check the menukeypress variable ,what typed in it
    if menukeypress == "+" :                        # if +, this is the case of ADD TO THE LIST
        print("Mit szertnél hozzáadni? -->")        # Print message
        addition_keypress = input()                 # Waiting for input, and put it into addition_keypress variable
        
        # open both files FOR APPEND, and write the additional content into the last lines
        with open('todo_items.txt', "a") as myfile:
            myfile.write("\n")
            myfile.write(addition_keypress)
        with open('checkList.txt', "a") as myfile2:
            myfile2.write("\n")
            myfile2.write("not finished")
        main()                                      # at the end of the operation, call back to the MAIN screen

    elif menukeypress == "quit" or menukeypress == "q": # if 'q' or 'quit' do a Program Termination Exit...
        print("Program elhagyása . . .\n\n")

    else:
        # IF NOT '+', not 'quit', maybe 1-2-3..., so handle the numbers 
        # This is not safe !! The case of letters not handled: What if user typed in a,b,c,d...etc?
        print ("számok lekezelése...")
        
        number = int(menukeypress)          # parse STRING --> to --> INTEGER
        # Variable PARSING is a MUST, to reach the 'checklist' numbered elements
        
        # check the checklist list of strings elements, if (not finished --> finished) AND (finished --> not finished)
        # this will replace the value in checklist variable, in memory(RAM) only!!!
        # number -1 element is for the difference between typed 1..10... and array numbering 0..9..
        if checklist[number-1] == "not finished":
           checklist[number-1] = "finished"
        elif checklist[number-1] == "finished":
            checklist[number-1] = "not finished"
        
        # After that, we need to write the variable contents into file, for permanent storage
        with open('checkList.txt', "w") as checkfile:
            item = 0                        # Initialize the first element, this is 0 !!
            # FULL OverWrite the file, from first element, until last element -1, and put the last \n
            # because the last item don't have to contain \n !!!
            for item in range(len(checklist) -1):
                checkfile.write("%s\n" % checklist[item])       # \n included until reach last element-1
            item += 1                                           # item = item + 1 ==> to reach te last element
            checkfile.write("%s" % checklist[item])             # DON'T PUT \n on the last element
        main()                                      # at the end of the operation, call back to the MAIN screen

def main():
    # Clear The Screen on Start
    os.system('clear')
    # Print Program Title
    print ("╔════════════════════════════════════════════════════════════════════╗")
    print ("║    <PactroSoft> ToDo Application - Professional Edition v1.02      ║")
    print ("╠════════════════════════════════════════════════════════════════════╣")
    # Initialize Global List Variables: LIST of Strings --> Get content from outer files
    todolist = get_todolist_from_file()     # Get ToDo list (strings) from get_todolist_from_file() method
    checklist = get_checklist_from_file()   # Get checklist list (strings) from get_checklist_from_file() method
    
    print_main_items(todolist,checklist)    # Call method with parameters pass
    handle_keypress(todolist,checklist)     # Call method with parameters pass

if __name__ == '__main__':
    main()
